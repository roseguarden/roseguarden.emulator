
import { Node } from '@/nodes/base'

class GenericNode extends Node{
    constructor(name) {
        super(name);
        this.name = name+"2";
    }
}
  
export { GenericNode };
