
import { ActionRequest, Action } from '@/generators/common'

export class BasicNodeActionRequest extends ActionRequest{
    constructor(fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        this.actions.push(new Action("", actioncounter+1));
    }
}
  

export class RegisterNodeStartupActionRequest extends ActionRequest{
    constructor(fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let startupAction = new Action("registerNodeStartup", actioncounter+1);
        this.actions.push(startupAction);
    }
}
  

export class SyncNodeIdentificationActionRequest extends ActionRequest{
    constructor(identityRaw, fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let identityAction = new Action("syncNodeIdentification", actioncounter+1);
        let parsedIdentity = JSON.parse(identityRaw);
        identityAction =  Object.assign(identityAction, parsedIdentity);
        this.actions.push(identityAction);
    }
}

export class SyncNodeLogActionRequest extends ActionRequest{
    constructor(logsRaw, fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let parsedLogs = JSON.parse(logsRaw);
        let logs = [];
        for (let i = 0; i < parsedLogs.length; i++) {
          let a = {};
          a.actionid = actioncounter + i + 1;
          a.version = "1.0.0";
          a.action = "syncNodeLog";
          let l = Object.assign(a, parsedLogs[i]);
          logs.push(l);
        }        
        this.actions = logs;
    }
}


export class SyncNodeSettingsActionRequest extends ActionRequest{
    constructor(settingsRaw, fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let settingsAction = new Action("syncNodeSettings", actioncounter+1);
        let parsedSettings = JSON.parse(settingsRaw);
        settingsAction =  Object.assign(settingsAction, parsedSettings);
        this.actions.push(settingsAction);
    }
}


export class RequestNodeUpdateActionRequest extends ActionRequest{
    constructor(fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let startupAction = new Action("requestNodeUpdate", actioncounter+1);
        this.actions.push(startupAction);
    }
}
  
