
import { ActionRequest, Action } from '@/generators/common'



export class RequestAssignCodeActionRequest extends ActionRequest{
    constructor(authenticator_key, fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let action = new Action("requestAssignCode", actioncounter+1);
        action.auth_key = authenticator_key;
        this.actions.push(action);
    }
}


export class RequestUserInfoActionRequest extends ActionRequest{
    constructor(authenticator_key, fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let action = new Action("requestUserInfo", actioncounter+1);
        action.auth_key = authenticator_key;
        this.actions.push(action);
    }
}


export class RequestUserAccessActionRequest extends ActionRequest{
    constructor(pin, authenticator_key, fingerprint, auth, actioncounter, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
        super(fingerprint, auth, logcounter, errorcounter, uptime, source, target, version);
        let action = new Action("requestUserAccess", actioncounter+1);
        if(pin && pin !== "") {
            action.pin = pin
        }
        action.auth_key = authenticator_key;
        this.actions.push(action);
    }
}
  


