

class Action{
    constructor(action, actionid , version="1.0.0") {
      this.version = version;
      this.action = action;
      this.actionid = actionid;
    }
}


class ActionRequest{ 
  constructor(fingerprint, auth, logcounter, errorcounter, uptime, source="Nodename", target="roseguarden.fabba.space", version="1.0.0") {
    let nowElapsed = Date.now();
    let now = new Date(nowElapsed);
    this.header = {};
    this.header.source = source;
    this.header.version = version;
    this.header.target = target;
    this.header.fingerprint = fingerprint;
    this.header.authentification = auth;
    this.header.uptime = uptime;
    this.header.logcounter = logcounter;
    this.header.errorcounter = errorcounter;
    this.header.timestamp = now.toISOString();
    this.actions = [];
  }

  dump() {
  }

  dumps() {
    return JSON.stringify(this,null,2);
  }
}

export { ActionRequest, Action };