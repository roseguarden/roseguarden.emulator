const Menu = [
  {header: 'Virtual nodes'},
  {
    title: 'Door',
    group: 'apps',
    icon: 'home',
    name: 'Door',
    href: 'door'
  },
  {
    title: 'Generic',
    group: 'apps',
    icon: 'dashboard',
    name: 'Generic',
    href: 'generic'
  }/*,
  {header: 'server'},
  {
    title: 'Log',
    group: 'apps',
    icon: 'feedback',
    name: 'Logs',
    href: 'log'
  },*/
];
// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
