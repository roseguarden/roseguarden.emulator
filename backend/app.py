from flask import Flask, request, render_template, send_from_directory, redirect, url_for
import logging
import datetime
import subprocess
import sys
import threading
import time
import json
import os
import traceback
from log import Logger

version = '0.0.1'
status = 'started'

# initialize flask app
app = Flask(__name__,
            static_folder = "./client")

logger = Logger(app)

# define routes for the flask app 
@app.route('/index')
@app.route('/')
def index():
    return send_from_directory('client', 'index.html')
    #return render_template("index.html")

@app.route('/<path:path>')
def static_proxy(path):
    # send_static_file will guess the correct MIME type
    if (os.path.isdir("./client/" + path)):
        return send_from_directory('client/' + path, 'index.html')
    elif (os.path.isfile("./client/" + path)):
        return app.send_static_file(path)
    else:
        return redirect(url_for('index'))

@app.route('/api/v1/log', methods=["GET"])
def getLog():
    log_string = ""
    for e in logger.logQueue:
        log_string = log_string + e + "\n"
    return log_string

@app.route('/api/v1/version', methods=["GET"])
def getVersion():
    return version

@app.route('/api/v1/updatestatus', methods=["GET"])
def getStatus():
    return status


# start background worker
if not app.debug or os.environ.get("WERKZEUG_RUN_MAIN") == "true":
    logger.info("Start rosenodemock v"+ str(version))
    #worker = threading.Thread(target=update_worker, args=(logger,))
    #worker.setDaemon(True)
    #worker.start()

if __name__ != '__main__':
    # get logging from unicorn
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    logger.info("Run app with gunicorn")

    # app.logger.addHandler(errhandler)

if __name__ == '__main__':
    logger.info("Run app locally (test)")
    app.run(host='0.0.0.0', port=5001, debug=True, use_reloader=False)


# wait for bacground worker to finish
# worker.join()


